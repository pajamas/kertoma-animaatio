import pygame
from math import sin

WHITE = (255,255,255)
RED = (255,0,0)
PINK = (255,200,210)
BLUE = (0,0,255)
YELLOW = (255,255,0)

pygame.init()
pygame.font.init()
my_font = pygame.font.SysFont(None, 40)
big_font = pygame.font.SysFont(None, 80)
small_font = pygame.font.SysFont(None, 25)

screen = pygame.display.set_mode((510,400))
FPS = pygame.time.Clock()

kertomat = [1]
for i in range(2,6):
    kertomat.append(i*kertomat[i-2])
print(kertomat)

sipuli_text = small_font.render("https://sipu.li/", True, (220,220,220))

start_text = big_font.render("Viiden kertoma", True, (0,0,0))
start_frames = 0

end_text = big_font.render("Kertoma laskettu!", True, (50,200,50))

kertoma_texts = [my_font.render(str(i), True, (0,0,0)) for i in kertomat]
current_kertoma = 0
kertoma_coords = [100, 200]
kertoma_gray_text = my_font.render("kertoma", True, (170,170,170))

blast_frames = 8
blast_images = [pygame.image.load("blast_" + str(int(i/2)+1) + ".png") for i in range(blast_frames)]
blast_phase = 0

multiply = big_font.render("*", True, (200,50,50))

plusOneText = my_font.render('+1', True, (200,50,50)) 

plus_ones = [[400, -30] for _ in range(5)]
current_plus_one = 0

current_i_coords = [400,200]
original_i_coords = list(current_i_coords)
current_i_texts = [my_font.render(str(i), True, (0,0,0)) for i in range(1,6)]
current_i = 0
i_dy = 8
i_gray_text = my_font.render("i", True, (170,170,170))

running = True
phase = "start"

change_i_break_frame = 0
before_multiply_frame = 0

"""
different animation phases:
start
plus_one_fall
change_i
change_i_break
before_multiply
multiply
after_multiply
end
"""

while running:
    FPS.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    screen.fill(WHITE)
    screen.blit(plusOneText, plus_ones[current_plus_one])
    screen.blit(kertoma_texts[current_kertoma], kertoma_coords)
    screen.blit(i_gray_text, [current_i_coords[0]+5, current_i_coords[1] + 30])
    screen.blit(kertoma_gray_text, [kertoma_coords[0]-30, kertoma_coords[1] - 30])
    screen.blit(current_i_texts[current_i], current_i_coords)

    if phase == "start":
        screen.fill(WHITE)
        screen.blit(start_text, [40,150])
        start_frames += 1
        if start_frames == 500:
            phase = "plus_one_fall"

    elif phase == "plus_one_fall":
        plus_ones[current_plus_one][1] += 4
        if (plus_ones[current_plus_one][1] >= current_i_coords[1]):
            current_plus_one += 1
            current_i += 1
            phase = "change_i"

    elif phase == "change_i":
        screen.blit(blast_images[blast_phase], (current_i_coords[0]-12, current_i_coords[1]-8))
        blast_phase += 1
        if blast_phase == blast_frames:
            phase = "change_i_break"
            blast_phase = 0

    elif phase == "change_i_break":
        change_i_break_frame += 1
        if change_i_break_frame == 10:
            phase = "before_multiply"
            change_i_break_frame = 0
        
    elif phase == "before_multiply":
        current_i_coords[0] -= 5.5
        current_i_coords[1] -= i_dy

        screen.blit(multiply, (200,250))

        i_dy -= 0.295

        if current_i_coords[0] < kertoma_coords[0]:
            phase = "multiply"
            i_dy = 8

    elif phase == "multiply":
        screen.blit(blast_images[blast_phase], (kertoma_coords[0]-12, kertoma_coords[1]-8))
        blast_phase += 1
        screen.blit(multiply, (200,250))
        if blast_phase == blast_frames:
            phase = "after_multiply"
            blast_phase = 0
            current_kertoma += 1

    elif phase == "after_multiply":
        screen.blit(multiply, (200,250))
        current_i_coords[0] += 5.5
        current_i_coords[1] -= i_dy

        i_dy -= 0.295

        if current_i_coords[0] >= original_i_coords[0]:
            current_i_coords = list(original_i_coords)
            if current_i == 4:
                phase = "end"
            else:
                phase = "plus_one_fall"
            i_dy = 8

    elif phase == "end":
        screen.blit(end_text, (15, 260))
        pygame.draw.circle(screen, (50,255,50), (kertoma_coords[0] + 22, kertoma_coords[1] + 12), 30, 3)

    screen.blit(sipuli_text,(10, 10))

    pygame.display.update()

